#include "ThreadPool.h"

std::once_flag ThreadPool::flag_;

ThreadPool::ThreadPool(int num):isStop_ ( false ) {
	for (int i = 0; i < num ; i++) {
		
		threads.emplace_back([this]( ) {

			while ( true ) {
				std::unique_lock<std::mutex>lock(mutex_);
				c_variable_.wait(lock, [ = ]() {
					return (!task_queue_.empty() || isStop_);
					});
				if ( isStop_  && task_queue_.empty() ) {  // 如果被线程池终止了
					return;
				}
				std::cout << "线程池处理" << std::endl;

				// 从任务队列，拿出任务执行
				std::function<void( )>task(std::move(task_queue_.front()));
				task_queue_.pop();
				task();
			}
			});
	}
}

ThreadPool::~ThreadPool( ) {
	{
		std::unique_lock<std::mutex>lock(mutex_);
		isStop_ = true;
		if (!task_queue_.empty())   c_variable_.notify_all();   // 唤醒所有线程去执行任务
	}

	for ( auto& a : threads ) {
		a.join();
	}
}

//// 往任务队列添加任务
//template <typename F, typename...  Agrs>
//void ThreadPool::push_task(F&& f, Agrs&&...  agrs) {
//	// 将函数和参数进行绑定
//	std::function<void()>task = std::bind(std::forward<F>(f), std::forward<Agrs>(agrs)...);
//	{
//		std::unique_lock<std::mutex>lock(mutex_);
//		// 加入任务队列
//		task_queue_.emplace(task);
//	}
//	c_variable_.notify_one();   // 唤醒一个线程来执行
//}