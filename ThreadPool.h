#ifndef _THREAD_POOL_H_
#define _THREAD_POOL_H_

#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <queue>
#include <functional>

class ThreadPool {
public:
	// 创建线程池单例
	static ThreadPool* getIntance(int num) {
		static ThreadPool* threadPool = nullptr;
		std::call_once(flag_, init_threadPool, threadPool, num);
		return threadPool;
	}

	// 往线程池中添任务
	template <typename F , typename...  Agrs>
	void push_task(F &&f , Agrs&&...  agrs);

private:
	static void init_threadPool(ThreadPool*& p, int num) {
		p = new ThreadPool(num);
	}

	ThreadPool(int num = 4);   // 默认线程数是 4 
	~ThreadPool();

	std::queue<std::function<void()>>task_queue_;   // 任务队列
	std::vector<std::thread>threads;              // 线程数组
	std::mutex mutex_;   // 互斥锁
	std::condition_variable c_variable_;  // 条件变量
	bool isStop_;        // 线程池是否终止
	static std::once_flag flag_;    // call_once()  需要的flag
};

// 往任务队列添加任务
template <typename F, typename...  Agrs>
void ThreadPool::push_task(F&& f, Agrs&&...  agrs) {
	// 将函数和参数进行绑定
	std::function<void()>task = std::bind(std::forward<F>(f), std::forward<Agrs>(agrs)...);
	{
		std::unique_lock<std::mutex>lock(mutex_);
		// 加入任务队列
		task_queue_.emplace(task);
	}
	c_variable_.notify_one();   // 唤醒一个线程来执行
}

#endif // !_THREAD_POOL_H



