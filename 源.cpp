#include "ThreadPool.h"
#include <iostream>
#include <algorithm>
#include <chrono>

void print(const char* name) {
	std::cout << "name :" << name << std::endl;
}

int getMax(int a, int b) {
	return std::max(a, b);
}

void sort(std::vector<int>*& value) {
	// 默认是升序
	std::sort(value->begin(), value->end());   // 
}

int main() {

	// 创建一个线程池 ，线程数为4
	ThreadPool* pool = ThreadPool::getIntance(4);
	std::vector<int>value;

	//for (int i = 1; i < 10; i++) {
	//	pool->push_task( [i]() {
	//		std::cout << "task runing " << i << std::endl;
	//		std::this_thread::sleep_for(std::chrono::seconds(1)); // 休眠1秒
	//		std::cout << "task stop : " << i << std::endl;
	//		});
	//}
	for (int i = 10; i > 0; i--) {
		value.emplace_back(i);
	}
	// 将任务投入线程池，让线程来处理
	pool->push_task(print, "小美");   // 输出小美
	pool->push_task(sort, &value);   // 升序排序

	std::this_thread::sleep_for(std::chrono::seconds(10)); // 休眠10秒

	for (int i = 0; i < value.size(); i++) {
		std::cout << value[i] << std::endl;
	}
	std::cout << "\n";

	return 0;
}